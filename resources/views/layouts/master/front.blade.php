<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>App Name | @yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('../public/favicon.ico') }}"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/fortawesome/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/driftyco/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/square/blue.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    @yield('content')
    @stack('scripts')
  </body>
</html>
