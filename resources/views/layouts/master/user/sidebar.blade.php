<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('/default_user.png') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{!! Auth::user()->first_name.' '.Auth::user()->last_name !!}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
  <!--====================================================================================-->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a href="{{ route('user_dashboard') }}">
          <i class="fa fa-key"></i> <span>Access Management</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="userlists"><i class="fa fa-users"></i> Users</a></li>
          <li><a href="{{ route('user_roles') }}"><i class="fa fa-gears"></i> Roles</a></li>
          <li><a href="{{ route('user_permissions') }}"><i class="fa fa-unlock-alt"></i> Permissions</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
  <!--====================================================================================-->
</aside>
