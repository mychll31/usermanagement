<!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    <h1>Account Activation</h1>
    <br>
    <b>SOMEONE NEED YOUR CONFIRMATION.</b><br />
    Below user just ask for your confirmation.<br />
    Name: {{ $last_name }}, {{ $first_name }}<br />
    Email Address: {{ $email }}<br />
    Please follow the link below to active the user account.<br />
    <br />
    {{ URL::to('register/activate/' . $confirmation_code) }}

    </body>
    </html>
