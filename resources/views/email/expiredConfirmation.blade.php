@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
<div class="login-box">
    <div class="login-logo">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg"><b>Opps sorry!!</b></p>
      Your confirmation code is <b>invalid</b>. Please just click the confirmation link in your email.<i class="fa fa-fw fa-frown-o"></i>
      <br>
      <br>{!! link_to('auth/register', $title = "Register a new membership") !!}
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection