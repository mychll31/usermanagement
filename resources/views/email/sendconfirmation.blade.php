@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
<div class="login-box">
    <div class="login-logo">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg"><b>CONGRATULATION!!</b></p>
      Thank you for registering. An email sent to your for your confirmation and to your superior for activation.
      <br>
      <br>{!! link_to('auth/login', $title = "Back to Login") !!}
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection