<!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Verify Your Email Address</h1>
        <b>CONGRATULATION! YOU ARE ONE STEP CLOSER.</b><br />
        Please follow the link below to verify your email address<br />
        {{ URL::to('register/verify/' . $confirmation_code) }}
    </body>
    </html>
