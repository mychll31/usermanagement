@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
<div class="login-box">
    <div class="login-logo">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg"><b>CONGRATULATION!!</b></p>
      Your account has been confirmed. Lets wait for your supervisor to activate your account so you can freely use the app. Thank you <i class="fa fa-fw fa-smile-o"></i>
      <br>
      <br>{!! link_to('auth/register', $title = "Register a new membership") !!}
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection