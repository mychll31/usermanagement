@extends('layouts.master.user.main')
@section('title', 'Users List')
@section('page_title', 'User Management')
@section('page_subtitle', '')
@section('content')
<link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet">
<link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">
<style>
#example1 tr {
    cursor: pointer;
}
#example1 tbody tr.highlight td {
    background-color: #9DD5F5;
}
#example1 tbody tr:hover{
    background-color: #9DD5F5 !important; 
}
</style>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Confirmed</th>
                        <th>Active</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                      <tr id="{{ $user->id }}" data-target="#myModal_{{ $user->id }}" data-toggle="modal" data-tooltip="{!! $user->first_name !!}" style="width: 41px; height:34px;" data_value="{{ $user->id }}">
                        <td>{!! ucfirst($user->last_name.', '.$user->first_name.' '.$user->middle_name[0]) !!}.</td>
                        <td>{!! strtolower($user->email) !!}</td>
                        <td>{!! ($user->admin == 1)? 'Admin' : 'User' !!}</td>
                        <td>{!! $user->created_at !!}</td>
                        <td>{!! $user->updated_at !!}</td>
                        <td>{!! ($user->confirmed == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                        <td>{!! ($user->active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                        @include('user.modal')
                      </tr>
                    @endforeach
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        
@endsection
@push('jscripts')
<!-- DataTables -->

  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
  <script src="{{ asset('/js/moment.js') }}"></script>

  <script>
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
    $('#example1').on('click', 'tbody tr', function(event) {
      $(this).addClass('highlight').siblings().removeClass('highlight');
      var trid = $(this).attr('id');
      console.log(trid);
      // $('#myModal').modal('show');
    });
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',

        });

      });
  </script>
@endpush