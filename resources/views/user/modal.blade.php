<!-- Modal -->
<div class="modal fade" id="myModal_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update User</h4>
      </div>
      <div class="modal-body">
      {!! Form::model($user, ['route' => ['user_update', $user->id]]) !!}
          {!! Form::token() !!}
            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
              {!! Form::text('first_name', $user->first_name, ['class'=>'form-control', 'placeholder'=>'First name', 'required' => 'required']) !!}
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('middle_name') ? 'has-error' : '' }}">
              {!! Form::text('middle_name', $user->middle_name, ['class'=>'form-control', 'placeholder'=>'Middle name', 'required' => 'required']) !!}
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              @if ($errors->has('middle_name')) <p class="help-block">{{ $errors->first('middle_name') }}</p> @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
              {!! Form::text('last_name', $user->last_name, ['class'=>'form-control', 'placeholder'=>'Last name', 'required' => 'required']) !!}
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
            </div>
            <div class="form-group has-feedback">
              {!! Form::email('email', $user->email, ['class'=>'form-control', 'placeholder'=>'Email', 'readonly' => 'true']) !!}
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group">
              {!! Form::checkbox('confirmed', $user->confirmed, ($user->confirmed==1) ? true: false) !!}
              &nbsp;&nbsp;Confirmed
              &nbsp;&nbsp;
              {!! Form::checkbox('active', $user->active, ($user->active==1) ? true: false) !!}
              &nbsp;&nbsp;Active
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          {!! Form::submit('Save changes', ['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>