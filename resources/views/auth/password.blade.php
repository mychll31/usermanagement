@extends('layouts.master.front')
@section('title', 'Forget Password')

@section('content')
<div class="login-box">
    <div class="login-logo">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Reset Password</p>
      {!! Form::open(['url' => 'password/email']) !!}
        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
          {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) !!}
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
        <div class="row">
          <div class="col-xs-8">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
          </div><!-- /.col -->
        </div>
      {!! Form::close() !!}
      <br>
      {!! link_to('auth/login', $title = "Back to Login") !!}
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection