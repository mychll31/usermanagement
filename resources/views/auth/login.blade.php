@extends('layouts.master.front')
@section('title', 'Login')

@section('content')
<div class="login-box">
    <div class="login-logo">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      {!! Form::open(['url' => 'auth/login']) !!}
      {!! Form::token() !!}
        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
          {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) !!}
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
          {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password', 'required' => 'required']) !!}
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                {!! Form::checkbox('remember', null, true) !!}  Remember Me
              </label>
            </div>
          </div><!-- /.col -->
          <div class="col-xs-4">
            {!! Form::submit('Sign In', ["class"=>"btn btn-primary btn-block btn-flat"]) !!}

          </div><!-- /.col -->
        </div>
      {!! Form::close() !!}
    <br>
    {!! link_to('password/email', $title = "I forgot my password") !!}
    <br>
    {!! link_to('auth/register', $title = "Register a new membership") !!}

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection
@push('scripts')
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
@endpush