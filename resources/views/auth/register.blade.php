@extends('layouts.master.front')
@section('title', 'Register')

@section('content')
<div class="register-box">
  <div class="register-logo">
    
  </div>
	<div class="register-box-body">
	  <p class="login-box-msg">Register a new membership</p>
	  <form action="{{ url('/auth/register') }}" method="post">
	  {!! Form::token() !!}
	    <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
	      {!! Form::text('first_name', old('first_name'), ['class'=>'form-control', 'placeholder'=>'First name', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('middle_name') ? 'has-error' : '' }}">
	      {!! Form::text('middle_name', old('middle_name'), ['class'=>'form-control', 'placeholder'=>'Middle name', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      @if ($errors->has('middle_name')) <p class="help-block">{{ $errors->first('middle_name') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
	      {!! Form::text('last_name', old('last_name'), ['class'=>'form-control', 'placeholder'=>'Last name', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
	    </div>
	    <br>
	    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
	      {!! Form::email('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	      @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
	      {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
	      {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Retype password', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
	      @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
	    </div>
	    <div class="row">
	      <div class="col-xs-8">
	        <div class="checkbox icheck">
	          <label>
	            {!! Form::checkbox('agree', old('agree'), true) !!}  I agree to the <a href="#" class='link_login'>terms</a>
	          </label>
	        </div>
	      </div><!-- /.col -->
	      <div class="col-xs-4">
	        {!! Form::submit('Register', ["class"=>"btn btn-primary btn-block btn-flat"]) !!}
	      </div><!-- /.col -->
	    </div>
	  {!! Form::close() !!}
	{!! link_to('auth/login', $title = "I already have a membership") !!}
	</div><!-- /.form-box -->
</div><!-- /.register-box -->
@endsection
@push('scripts')
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
@endpush