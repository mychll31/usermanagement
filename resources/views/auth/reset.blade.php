@extends('layouts.master.front')
@section('title', 'Reset Password')

@section('content')
<div class="register-box">
  <div class="register-logo">
    
  </div>
	<div class="register-box-body">
	  <p class="login-box-msg">Reset Password</p>
	  {!! Form::open(['to' => 'password/reset', 'role' => 'form']) !!}
	  	<input type="hidden" name="token" value="{{ $token }}">
	    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
	      {!! Form::input('email', 'email', old('email'), ['class'=>'form-control', 'placeholder'=>'Email', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	      @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
	      {!! Form::input('password', 'password', null, ['class'=>'form-control', 'placeholder'=>'Password', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
	    </div>
	    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
	      {!! Form::input('password', 'password_confirmation', null, ['class'=>'form-control', 'placeholder'=>'Retype password', 'required' => 'required']) !!}
	      <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
	      @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
	    </div>
	    <div class="row">
	      <div class="col-xs-8">
	      {!! Form::submit('Reset Password', ["class"=>"btn btn-primary btn-block btn-flat"]) !!}
	      </div>
	    </div>
	  {!! Form::close() !!}
	</div><!-- /.form-box -->
</div><!-- /.register-box -->
@endsection
@push('scripts')
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
@endpush