<?php
/**
 * Created by PhpStorm.
 * User: malcorin
 * Date: 6/26/2015
 * Time: 3:08 PM
 */

use App\User;

class UserTableSeeder extends  DatabaseSeeder{

    public function run()
    {
        DB::table('users')->delete();
        $records = [
            [
                'first_name'    => str_random(10),
                'middle_name'   => str_random(10),
                'last_name'     => str_random(10),
                'email'         => str_random(8).'@gmail.com',
                'password'      => bcrypt('secret'),
                'confirmed'     => 1,
                'confirmation_code' => '',
                'active'         => 1,
                'admin'          => 0,
                'created_at'     => '2015-06-26 10:23:00'
            ]
        ];

        User::insert($records);
    }
}
