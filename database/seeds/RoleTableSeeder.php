<?php



use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RoleTableSeeder extends Seeder{

    public function run()
    {
        DB::table('roles')->delete();
        $records = [
            [
                'name'          => 'Root',
                'description'   => 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.'
            ],
            [
                'name'          => 'Administrator',
                'description'   => 'Full access to create, edit, and update companies, and orders.'
            ],
            [
                'name'          => 'Manager',
                'description'   => 'Ability to create new companies and orders, or edit and update any existing ones.'
            ],
            [
                'name'          => 'Company Manager',
                'description'   => 'Able to manage the company that the user belongs to, including adding sites, creating new users and assigning licences.'
            ],
            [
                'name'          => 'User',
                'description'   => 'A standard user that can have a licence assigned to them. No administrative features.'
            ]
        ];

        Role::insert($records);
    }

}