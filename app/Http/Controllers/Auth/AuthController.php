<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;
use Hash;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $redirectPath = '/';
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(30).Hash::make(User::count());
        User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code,
        ]);

        # verification for user
        Mail::send('email.verify', ['confirmation_code' => $confirmation_code], function($message) use ($data)
        {
            $message->from('no-reply@webmaster.com', "ACCOUNT VERIFICATION");
            $message->subject("CRONUS VERIFICATION");
            $message->to($data['email']);
        });

        # activation of account
        Mail::send('email.activation', ['first_name' => $data['first_name'], 'last_name' => $data['last_name'], 'email' => $data['email'], 'confirmation_code' => $confirmation_code], function($message) use ($data)
        {
            $message->from('no-reply@webmaster.com', "ACCOUNT APPROVAL");
            $message->subject("CRONUS Approval");
            //TODO:: Create approval email address
            $message->to('testlevaral@gmail.com');
        });
    }
}
