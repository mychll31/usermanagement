<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class UserController extends Controller
{
    public function dashboard(){
    	return view('user.dashboard');
    }

    public function lists(){
    	$users = User::all();
    	return view('user.lists', compact('users'));
    }

    public function update(Request $request){
        $user = User::where('email', $request->email)->first();
        $user->first_name   = $request->first_name;
        $user->middle_name  = $request->middle_name;
        $user->last_name    = $request->last_name;
        $user->confirmed    = $request->has('confirmed')? 1 : 0;
        $user->active       = $request->has('active')? 1 : 0;
        $user->save();
        return redirect('userlists');
    }

    public function roles(){
    	return view('user.dashboard');
    }

    public function permissions(){
    	$user = Auth::user();
    	return view('user.dashboard');
    }
}
