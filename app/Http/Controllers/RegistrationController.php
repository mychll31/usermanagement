<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class RegistrationController extends Controller
{
    public function confirm($confirmation_code)
    {

        if( ! $confirmation_code)
        {
            return view('email.expiredConfirmation');
            // throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();
        if ( ! $user)
        {
            return view('email.expiredConfirmation');
            // throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->save();
        if($user->active == 0){
            return view('email.confirmation');
        }else{
            $user->confirmation_code = null;
            $user->save();
            return redirect('auth/login');
        }

    }

    public function activate($confirmation_code)
    {

        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->active = 1;
        $user->save();

    }

}
