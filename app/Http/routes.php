
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('user/{user}', [
    'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
    'uses' => 'UserController@index',
    'roles' => ['administrator', 'manager'] // Only an administrator, or a manager can access this route
]);

/* USER LOGIN AND REGISTRATION*/
Route::get('/', function () {
    return redirect('auth/login');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'register/'], function () {
    Route::get('verify/{confirmationCode}',	[
    	'as'	=> 'confirmation_path',
    	'uses'	=> 'RegistrationController@confirm'
    ]);
    
    Route::get('activate/{confirmationCode}', [
    	'as'	=> 'activation_path',
    	'uses'	=> 'RegistrationController@activate'
    ]);

    Route::get('sendconfirmation', function() {
        return view('email.sendconfirmation');
    });

    Route::get('sendpassword', function() {
        return redirect('auth/login');
    });

});
/* END - USER LOGIN AND REGISTRATION*/

/* MANAGE USER */
// TODO:: homepage
Route::get('/home', function () {
    return redirect('userlists');
});

// Route::group(function () {
    Route::get('dashboard', [
        'as'    => 'user_dashboard',
        'uses'  => 'UserController@dashboard'
    ]);

Route::get('userlists', [
    'as'    => 'user_lists',
    // 'middleware' => 'admin',
    'uses'  => 'UserController@lists'
]);
Route::post('userupdate', [
    'as'    => 'user_update',
    'uses'  => 'UserController@update'
]);

    Route::get('roles', [
        'as'    => 'user_roles',
        'uses'  => 'UserController@roles'
    ]);
    Route::get('permissions', [
        'as'    => 'user_permissions',
        'uses'  => 'UserController@permissions'
    ]);
// });
/* END - MANAGE USER */

use App\Message;
use App\User;
get('userlist', function() {
    return view('user.lists');
});
/*API*/
get('api/users', function() {
    return User::all();
});

post('api/users', function() {
    return Message::create(Request::all());
});
/*END - API*/


get('guestbook', function() {
    return view('guestbook');
});

// API

get('api/messages', function() {
    return Message::all();
});

post('api/messages', function() {
    return Message::create(Request::all());
});